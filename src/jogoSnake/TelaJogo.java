package jogoSnake;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JPanel;

public class TelaJogo extends JPanel implements Runnable, KeyListener
{
	private static final long serialVersionUID = 1L;
	public static final int LARGURA = 500;
	public static final int ALTURA = 500;
	
	//Render
	private Graphics2D g2d;
	private BufferedImage image;
	
	//GameLoop
	private Thread thread;
	private boolean jogoRodando;
	private long targetTime;
	private int timerComida = 0;
	private int timerObstaculo = 0;
	
	//Variáveis uteis do jogo
	private String tipoJogo;
	private final int TAMANHO = 10;
	private Pixel cabeca, obstaculo;
	private Comida comida;
	private ArrayList<Pixel> cobra;
	private int pontuacao;
	private int level;
	private boolean gameOver;
	
	//movimento
	private int dx, dy;
	
	//entrada do usuário
	private boolean cima, baixo, direita, esquerda, comecar;
	
	public TelaJogo(String tipoJogo) 
	{
		this.tipoJogo = tipoJogo;
		setPreferredSize(new Dimension(LARGURA, ALTURA));
		setFocusable(true);
		requestFocus();
		addKeyListener(this);
	}
	
	@Override
	public void addNotify() {		
		super.addNotify();
		thread = new Thread(this);
		thread.start();
	}
	
	//Velocidade em que a tela do jogo é atualizada
	private void setFPS(int fps)
	{
		targetTime = 1000/fps;
	}
	
	@Override
	public void keyPressed(KeyEvent e)
	{
		int k = e.getKeyCode();

		if(k == KeyEvent.VK_UP)
			cima = true;
		if(k == KeyEvent.VK_DOWN)
			baixo = true;
		if(k == KeyEvent.VK_LEFT)
			esquerda = true;
		if(k == KeyEvent.VK_RIGHT)
			direita = true;
		if(k == KeyEvent.VK_ENTER)
			comecar = true;
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		int k = e.getKeyCode();
		
		if(k == KeyEvent.VK_UP)
			cima = false;
		if(k == KeyEvent.VK_DOWN)
			baixo = false;
		if(k == KeyEvent.VK_LEFT)
			esquerda = false;
		if(k == KeyEvent.VK_RIGHT)
			direita = false;
		if(k == KeyEvent.VK_ENTER)
			comecar = false;
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
		

	}

	@Override
	public void run()
	{
		if(jogoRodando)
			return;
		
		init();
		long tempoInicial;
		long tempoDecorrido;
		long espera;
		while(jogoRodando)
		{
			timerComida++;
			timerObstaculo++;
			
			if(timerComida == 70)
			{
				if(!gameOver)
				{
					comida = null;
					criaComida();
					timerComida = 0;
				}
			}
			
			if(timerObstaculo == 70)
			{
				if(!gameOver)
				{
					obstaculo = null;
					criaObstaculo();
					timerObstaculo = 0;
				}
			}
			
			tempoInicial = System.nanoTime();
			
			update();
			requestRender();
			
			tempoDecorrido = System.nanoTime() - tempoInicial;
			espera = targetTime - tempoDecorrido / 1000000;
			if(espera > 0)
			{
				try
				{
					Thread.sleep(espera);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	//Inicialização do jogo
	private void init()
	{
		image = new BufferedImage(LARGURA, ALTURA, BufferedImage.TYPE_INT_ARGB);
		g2d = image.createGraphics();
		jogoRodando = true;
		setUplevel();
	}
	
	//Prepara o jogo para o modelo inicial
	private void setUplevel()
	{
		cobra = new ArrayList<Pixel>();
		cabeca = new Pixel(TAMANHO);
		cabeca.setPosicao(LARGURA / 2, ALTURA / 2);
		cobra.add(cabeca);
		for(int i = 1; i < 3; i++)
		{
			Pixel e = new Pixel(TAMANHO);
			e.setPosicao(cabeca.getX() + (i * TAMANHO), cabeca.getY());
			cobra.add(e);
		}
		comida = new Comida(TAMANHO);
		obstaculo = new Pixel(TAMANHO);
		criaComida();
		timerObstaculo = 0;
		criaObstaculo();
		pontuacao = 0;
		gameOver = false;
		level = 1;
		dx = dy = 0;
		setFPS(level * 10);
	}
	
	public void criaComida()
	{
		int tipoComida = (int)(Math.random() * 4 + 1);
				
		int x = (int)(Math.random() * (LARGURA - TAMANHO));
		int y = (int)(Math.random() * (ALTURA - TAMANHO));
		x = x - (x % TAMANHO);
		y = y - (y % TAMANHO);
		
		comida = new Comida(TAMANHO);
		
		if(tipoComida == 2)
			comida.setTipoComida("Bomba");
		else if(tipoComida == 3)
			comida.setTipoComida("Big");
		else if(tipoComida == 4)
			comida.setTipoComida("Diminui");
		else
			comida.setTipoComida("Comum");
		
		comida.setPosicao(x, y);
	}
	
	public void criaObstaculo()
	{
		int x = (int)(Math.random() * (LARGURA - TAMANHO));
		int y = (int)(Math.random() * (ALTURA - TAMANHO));
		x = x - (x % TAMANHO);
		y = y - (y % TAMANHO);
		
		obstaculo = new Pixel(TAMANHO);
		
		obstaculo.setPosicao(x, y);
	}
	
	private void requestRender()
	{
		render(g2d);
		Graphics g = getGraphics();
		g.drawImage(image,0,0,null);
		g.dispose();
		
	}
	
	//Atualiza o jogo na frequencia que o 'setFPS' determina
	private void update()
	{
		if(gameOver)
		{
			if(comecar)
			{
				setUplevel();
			}
			return;
		}
		if(cima && dy == 0)
		{
			dy = -TAMANHO;
			dx = 0;
		}
		if(baixo && dy == 0)
		{
			dy = TAMANHO;
			dx = 0;
		}
		if(esquerda && dx == 0)
		{
			dy = 0;
			dx = -TAMANHO;
		}
		if(direita && dx == 0 && dy != 0)
		{
			dy = 0;
			dx = TAMANHO;
		}
		if(dx != 0 || dy != 0)
		{
			for(int i = cobra.size() - 1; i > 0; i--)
			{
				cobra.get(i).setPosicao(cobra.get(i - 1).getX(), cobra.get(i - 1).getY());
			}
			cabeca.mover(dx, dy);
		}
		
		for(Pixel e : cobra)
		{
			if(e.isCollision(cabeca))
			{
				gameOver = true;
				break;
			}
		}
		
		if(comida.isCollision(cabeca))
		{
			if(comida.getTipoComida() == "Comum")
			{
				if(tipoJogo == "Star")
					pontuacao += 2;
				else
					pontuacao++;
				
				
				timerComida = 0;
				
				criaComida();
				
				Pixel e = new Pixel(TAMANHO);
				e.setPosicao(-100, -100);
				cobra.add(e);
				if(pontuacao % 10 == 0)
				{
					level++;
					if(level > 10)
						level = 10;
					setFPS(level * 10);
				}
			}
			else if(comida.getTipoComida() == "Bomba")
			{
				gameOver = true;
				
				timerComida = 0;
			}
			else if(comida.getTipoComida() == "Big")
			{
				pontuacao += 2;
				if(tipoJogo == "Star")
					pontuacao += 2;
				
				
				timerComida = 0;
				criaComida();
				
				Pixel e = new Pixel(TAMANHO);
				e.setPosicao(-100, -100);
				cobra.add(e);
				if(pontuacao % 10 == 0)
				{
					level++;
					if(level > 10)
						level = 10;
					setFPS(level * 10);
				}
			}
			else if(comida.getTipoComida() == "Diminui")
			{
				for(int i = cobra.size() - 1; i >= 3; i--)
				{
					cobra.remove(i);
				}
				
				timerComida = 0;
				criaComida();
			}
		}
		
		if(obstaculo.isCollision(cabeca))
		{
			if(tipoJogo != "Kitty")
			{
				gameOver = true;
				timerComida = 0;
			}
		}
		
		if(cabeca.getX() < 0)
			cabeca.setX(LARGURA);
		if(cabeca.getY() < 0)
			cabeca.setY(ALTURA);
		if(cabeca.getX() > LARGURA)
			cabeca.setX(0);
		if(cabeca.getY() > ALTURA)
			cabeca.setY(0);
		
	}
	
	//Desenha os pixels na tela
	public void render(Graphics2D g2d)
	{
		if(tipoJogo == "Comum")
		{
			g2d.clearRect(0, 0, LARGURA, ALTURA);
			
			g2d.setColor(Color.GREEN);
			for(Pixel e : cobra)
			{
				e.render(g2d);
			}
			
			if(comida.getTipoComida() == "Comum")
			{
				g2d.setColor(Color.RED);
				comida.render(g2d);
			}
			else if(comida.getTipoComida() == "Bomba")
			{
				g2d.setColor(Color.GRAY);
				comida.render(g2d);
			}
			else if(comida.getTipoComida() == "Big")
			{
				g2d.setColor(Color.CYAN);
				comida.render(g2d);
			}
			else if(comida.getTipoComida() == "Diminui")
			{
				g2d.setColor(Color.ORANGE);
				comida.render(g2d);
			}			
			
			g2d.setColor(Color.BLUE);
			obstaculo.render(g2d);
			if(gameOver)
			{
				g2d.setColor(Color.WHITE);
				g2d.drawString("Game Over!", 225, 250);
			}
					
			g2d.setColor(Color.WHITE);
			g2d.drawString("Pontuação: " + pontuacao + " Nível: " + level, 10, 10);
			if(dx == 0 && dy == 0)
			{
				g2d.drawString("Pronto!", 225, 250);
			}
		}
		else if(tipoJogo == "Kitty")
		{
			g2d.clearRect(0, 0, LARGURA, ALTURA);
			
			g2d.setColor(Color.MAGENTA);
			for(Pixel e : cobra)
			{
				e.render(g2d);
			}
			
			if(comida.getTipoComida() == "Comum")
			{
				g2d.setColor(Color.RED);
				comida.render(g2d);
			}
			else if(comida.getTipoComida() == "Bomba")
			{
				g2d.setColor(Color.GRAY);
				comida.render(g2d);
			}
			else if(comida.getTipoComida() == "Big")
			{
				g2d.setColor(Color.CYAN);
				comida.render(g2d);
			}
			else if(comida.getTipoComida() == "Diminui")
			{
				g2d.setColor(Color.ORANGE);
				comida.render(g2d);
			}

			g2d.setColor(Color.BLUE);
			obstaculo.render(g2d);
			if(gameOver)
			{
				g2d.setColor(Color.WHITE);
				g2d.drawString("Game Over!", 225, 250);
			}
					
			g2d.setColor(Color.WHITE);
			g2d.drawString("Pontuação: " + pontuacao + " Nível: " + level, 10, 10);
			if(dx == 0 && dy == 0)
			{
				g2d.drawString("Pronto!", 225, 250);
			}
		}
		else if(tipoJogo == "Star")
		{
			g2d.clearRect(0, 0, LARGURA, ALTURA);
			
			g2d.setColor(Color.YELLOW);
			for(Pixel e : cobra)
			{
				e.render(g2d);
			}
			
			if(comida.getTipoComida() == "Comum")
			{
				g2d.setColor(Color.RED);
				comida.render(g2d);
			}
			else if(comida.getTipoComida() == "Bomba")
			{
				g2d.setColor(Color.GRAY);
				comida.render(g2d);
			}
			else if(comida.getTipoComida() == "Big")
			{
				g2d.setColor(Color.CYAN);
				comida.render(g2d);
			}
			else if(comida.getTipoComida() == "Diminui")
			{
				g2d.setColor(Color.ORANGE);
				comida.render(g2d);
			}

			g2d.setColor(Color.BLUE);
			obstaculo.render(g2d);
			if(gameOver)
			{
				g2d.setColor(Color.WHITE);
				g2d.drawString("Game Over!", 225, 250);
			}
					
			g2d.setColor(Color.WHITE);
			g2d.drawString("Pontuação: " + pontuacao + " Nível: " + level, 10, 10);
			if(dx == 0 && dy == 0)
			{
				g2d.drawString("Pronto!", 225, 250);
			}
		}
	}
}
