package jogoSnake;

import java.awt.Dimension;

import javax.swing.JFrame;

public class Main {

	public static void main(String[] args)
	{
		JFrame menu = new JFrame("Menu Principal");
		menu.setContentPane(new Menu());
		menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		menu.setResizable(false);
		menu.pack();
		menu.setPreferredSize(new Dimension(400,400));
		menu.setLocationRelativeTo(null);
		menu.setVisible(true);
		
	}
}
