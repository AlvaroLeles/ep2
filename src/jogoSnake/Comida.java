package jogoSnake;

public class Comida extends Pixel {

	private String tipoComida;
	
	public Comida(int tamanho) {
		super(tamanho);
		tipoComida = "Comum";
	}
	
	public String getTipoComida()
	{
		return tipoComida;
	}
	
	public void setTipoComida(String tipoComida)
	{
		this.tipoComida = tipoComida;
	}

}
