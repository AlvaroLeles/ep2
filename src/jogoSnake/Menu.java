package jogoSnake;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class Menu extends JPanel{	
	{
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    } 
		catch (Exception ex) {
	    	ex.printStackTrace();
	    }
		
		JPanel newPanel = new JPanel();
		newPanel.setLayout(new GridBagLayout());
        JButton buttonTipo1 = new JButton("Comum");
        JButton buttonTipo2 = new JButton("Kitty");
        JButton buttonTipo3 = new JButton("Star");
         
 
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
          
        // Adiciona componentes ao panel
          
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.CENTER;
        
        newPanel.add(buttonTipo1, constraints);
        constraints.gridy = 8;
        newPanel.add(buttonTipo2, constraints);
        constraints.gridy = 14;
        newPanel.add(buttonTipo3, constraints);
        newPanel.setPreferredSize(new Dimension(400,400));
        
        //Abre o jogo no modo comum
        buttonTipo1.addActionListener(new ActionListener() {

        	@Override
			public void actionPerformed(ActionEvent arg0) {
				newPanel.setVisible(false);
				JFrame frame = new JFrame("Snake");
				frame.setContentPane(new TelaJogo(buttonTipo1.getText()));
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setResizable(false);
				frame.pack();
				frame.setPreferredSize(new Dimension(TelaJogo.LARGURA, TelaJogo.ALTURA));
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				
			}
		});
        
        //Abre o jogo no modo Kitty
        buttonTipo2.addActionListener(new ActionListener() {

        	@Override
			public void actionPerformed(ActionEvent arg0) {
				newPanel.setVisible(false);
				JFrame frame = new JFrame("Snake");
				frame.setContentPane(new TelaJogo(buttonTipo2.getText()));
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setResizable(false);
				frame.pack();
				frame.setPreferredSize(new Dimension(TelaJogo.LARGURA, TelaJogo.ALTURA));
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				
			}
		});
        
      //Abre o jogo no modo Star
        buttonTipo3.addActionListener(new ActionListener() {

        	@Override
			public void actionPerformed(ActionEvent arg0) {
				newPanel.setVisible(false);
				JFrame frame = new JFrame("Snake");
				frame.setContentPane(new TelaJogo(buttonTipo3.getText()));
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setResizable(false);
				frame.pack();
				frame.setPreferredSize(new Dimension(TelaJogo.LARGURA, TelaJogo.ALTURA));
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				
			}
		});
        
        // Define borda ao panel
        newPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Jogo Snake"));
          
        // Adiciona o panel ao frame
        add(newPanel);
          
	}
}
